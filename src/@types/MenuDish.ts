import { Dish } from "@/@types";
import { MenuType } from "@/@types";

export interface MenuDish {
    id: string
    price: number
    dishes: Dish[]
    menu: MenuType
    created_at: string
    updated_at: string
}

export interface StateMenuDish {
    menuDishes: MenuDish[]
    menuDish: MenuDish
}
