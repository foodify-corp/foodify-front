import { Dish } from "@/@types/Dish";
import { Category } from "@/@types/Category";

export interface CategoryDish {
    id: string
    dishes: Dish[]
    category: Category
    created_at: string
    updated_at: string
}

export interface StateCategoryDish {
    categoryDish: CategoryDish
    categoriesDishes: CategoryDish[]
}
