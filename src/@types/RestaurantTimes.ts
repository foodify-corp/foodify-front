export interface RestaurantTimes {
    id: string
    mondayOpenAt: number | null
    mondayCloseAt: number | null
    tuesdayOpenAt: number | null
    tuesdayCloseAt: number | null
    wednesdayOpenAt: number | null
    wednesdayCloseAt: number | null
    thursdayOpenAt: number | null
    thursdayCloseAt: number | null
    fridayOpenAt: number | null
    fridayCloseAt: number | null
    saturdayOpenAt: number | null
    saturdayCloseAt: number | null
    sundayOpenAt: number | null
    sundayCloseAt: number | null
}

export interface StateRestaurantTimes {
    restaurantTimes: RestaurantTimes
}
