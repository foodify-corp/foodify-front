export interface Comment {
    id: string
    note: number,
    description: string,
    created_at: string,
    updated_at: string,
    username: string
}

export interface StateComment {
    comments: Comment[]
}
