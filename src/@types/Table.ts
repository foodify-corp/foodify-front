import { Reservation } from "@/@types/Reservation";

export interface Table {
    id: string
    table_numero: number
    nb_place: number
    available: boolean
    reservations: Reservation[]
    lunch_service: string
    evening_service: string
    created_at: string
    updated_at: string
}

export interface StateTable {
    tables: Table[],
    selectedTable: {
        id: string,
        table_numero: number
        nb_place: number
        available: boolean
        reservations: Reservation[]
        lunch_service: string
        evening_service: string
        created_at: string
        updated_at: string
    }
}
