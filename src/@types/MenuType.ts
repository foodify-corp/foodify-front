export interface MenuType {
    id: string
    name: string
    display: boolean
    created_at: string
    updated_at: string
}

export interface StateMenu {
    menus: MenuType[]
    menu: MenuType
}
