import { OrderDish, Dish } from "@/@types";

export interface Order {
    id: string
    order_type: string
    order_dishes: OrderDish[]
    created_at: string
    updated_at: string
}

export interface StateOrder {
    currentOrder: Order
    orderedDishes: Dish[]
}
//TODO check if interface still up to date after Michael made changes
