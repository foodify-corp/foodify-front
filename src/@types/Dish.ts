export interface Dish {
    id: string
    name: string
    price: number
    is_available: boolean
    is_available_time: string[]
    created_at: string
    updated_at: string
    dishes_categories: string[]
    restaurants_id: string

}

export interface StateDish {
    dishes: Dish[],
    selectedDish: Dish
}

//TODO Interface need to be checked