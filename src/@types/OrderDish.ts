import { Dish } from "@/@types/Dish";

export interface OrderDish {
    quantity: number
    dish: Dish
    created_at: string
    updated_at: string
}
//TODO check if interface still up to date after Michael made changes
