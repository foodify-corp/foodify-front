import { createStore } from 'vuex';
import {
  StateUser,
  StateDish,
  StateOrder,
  StateRestaurant,
  StateReservation,
  StateCategory,
  StateTable,
  StateMenu,
  StateMenuDish,
  StateCategoryDish,
  StateRestaurantTimes,
  StateComment
} from '@/@types';
import users from './modules/users.store';
import dishes from './modules/dishes.store';
import orders from './modules/orders.store';
import restaurants from './modules/restaurants.store'
import reservations from './modules/reservations.store'
import categories from './modules/category.store'
import tables from './modules/tables.store'
import menus from './modules/menus.store'
import menuDish from './modules/menuDish.store'
import categoryDish from './modules/categoryDish.store'
import restaurantTimes from './modules/restaurantTimes.store'
import comments from './modules/comments.store'

export default createStore({
  modules: {
    users,
    dishes,
    orders,
    restaurants,
    reservations,
    categories,
    tables,
    menus,
    menuDish,
    categoryDish,
    restaurantTimes,
    comments
  }
})

export interface RootState {
  users: StateUser
  restaurants: StateRestaurant
  reservations: StateReservation
  dishes: StateDish
  orders: StateOrder
  categories: StateCategory,
  tables: StateTable,
  menus: StateMenu,
  menuDish: StateMenuDish,
  categoryDish: StateCategoryDish,
  restaurantTimes: StateRestaurantTimes,
  comments: StateComment
}
