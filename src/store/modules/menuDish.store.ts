import { Dish, MenuDish, StateMenuDish } from "@/@types";
import { ActionContext, ActionTree, GetterTree, MutationTree } from "vuex";
import { RootState } from "@/store";
import api from "@/services/api/ApiService";

const state: StateMenuDish = {
    menuDishes: [],
    menuDish: {
        id: '',
        menu: {
            id: '',
            name: '',
            display: false,
            created_at: '',
            updated_at: ''
        },
        dishes: [],
        price: 0,
        created_at: '',
        updated_at: ''
    }
}

const getters: GetterTree<StateMenuDish, RootState> = {
    menuDish: state => state.menuDish,
    menuDishes: state => state.menuDishes,
    menuPrice: state => (menuId: string) => {
        const menu = state.menuDishes.find(menuDish => {
            return menuDish.menu.id === menuId;
        });
        if (menu) {
            return menu.dishes.reduce((acc: any, sum) => {
                acc += sum.price;
                return acc;
            }, 0);
        }
    }
}

const mutations: MutationTree<StateMenuDish> = {
    SET_MENU_DISH(state: StateMenuDish, menuDish: MenuDish) {
        state.menuDish = menuDish;
    },
    SET_ALL_MENU_DISHES(state: StateMenuDish, menuDishes: MenuDish[]) {
        state.menuDishes = menuDishes;
    },
    ADD_MENU_DISH(state: StateMenuDish, menuDishes: Dish[]) {
        state.menuDish.dishes.push(menuDishes[0]);
        state.menuDish.price += menuDishes[0].price;
    },
    ADD_MENU_DISHES(state: StateMenuDish, payload: { menuDishes: Dish[], menuId: string }) {
        const menu = state.menuDishes.find(menuDish => {
            return menuDish.menu.id === payload.menuId;
        });
        if(menu) {
            menu.dishes.push(payload.menuDishes[0]);
        }
    },
    DELETE_MENU_DISH(state: StateMenuDish, deletedDishId: String) {
        // @ts-ignore
        const dishToDelete: Dish = state.menuDish.dishes.find(dish => dish.id === deletedDishId);
        const dishIndexToDelete = state.menuDish.dishes.indexOf(dishToDelete);
        state.menuDish.dishes.splice(dishIndexToDelete, 1);
        state.menuDish.price -= dishToDelete.price;
    },
    DELETE_MENU_DISHES(state: StateMenuDish, payload: { deletedDishId: string, menuId: string }) {
        const menu = state.menuDishes.find(menuDish => {
            return menuDish.menu.id === payload.menuId;
        });
        if(menu) {
            // @ts-ignore
            const dishToDelete: Dish = menu.dishes.find(dish => dish.id === payload.deletedDishId);
            const dishIndexToDelete: number = menu.dishes.indexOf(dishToDelete);
            menu.dishes.splice(dishIndexToDelete, 1);
            menu.price -= dishToDelete.price;
        }
    }
}

const actions: ActionTree<StateMenuDish, RootState> = {
    async getMenuDishes({ commit }: ActionContext<StateMenuDish, RootState>, payload) {
        const res = await api.getMenuDishes(payload.restaurantId, payload.menuId);
        commit('SET_MENU_DISH', res.menu_dishes);
        return res;
    },
    async getAllMenuDishes({ commit }: ActionContext<StateMenuDish, RootState>, restaurantId: string) {
        const res = await api.getAllMenuDishes(restaurantId);
        commit('SET_ALL_MENU_DISHES', res.menu_dishes);
        return res;
    },
    async addMenuDishes({ commit }: ActionContext<StateMenuDish, RootState>, payload) {
        const res = await api.addMenuDishes(payload.restaurantId, payload.menuId, payload.dishesId);
        commit('ADD_MENU_DISH', res.menu_dishes);
        commit('ADD_MENU_DISHES', { menuDishes: res.menu_dishes, menuId: payload.menuId });
        return res;
    },
    async deleteMenuDishes({ commit }: ActionContext<StateMenuDish, RootState>, payload) {
        const res = await api.deleteMenuDishes(payload.restaurantId, payload.menuId, payload.dishId);
        commit('DELETE_MENU_DISH', res.deleted_dish_id);
        commit('DELETE_MENU_DISHES', { deletedDishId: res.deleted_dish_id, menuId: payload.menuId });
        return res;
    }
}

export default {
    namespaced: true,
    state,
    getters,
    mutations,
    actions
}
