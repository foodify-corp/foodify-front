import { ActionContext, ActionTree, GetterTree, MutationTree } from "vuex";
import { RootState } from "@/store";
import api from "@/services/api/ApiService";
import {Comment, StateComment} from "@/@types";

const state: StateComment = {
    comments: []
}

const getters: GetterTree<StateComment, RootState> = {
    comments: (state: StateComment) => state.comments,
}

const mutations: MutationTree<StateComment> = {
    COMMENTS(state, comments: Comment[]) {
        state.comments = comments;
    },
    ADD_COMMENT(state, comment: Comment) {
        state.comments.unshift(comment);
    }
}

const actions: ActionTree<StateComment, RootState> = {
    async getComments({commit}: ActionContext<StateComment, RootState>, restaurantId: string) {
        try {
            const res = await api.getComments(restaurantId);
            const restoComments = res.comments;
            let comments = restoComments.map((restoComment: any) => {
                const comment = restoComment.comment[0];
                const username = comment.user_id.email.split("@")[0];
                delete comment.user_id;
                comment.username = username;
                return comment;
            });
            comments.sort((a,b) => new Date(b.updated_at) - new Date(a.updated_at));
            commit('COMMENTS', comments);
        } catch (e: any) {
            throw e.data;
        }
    },
    async addComment({commit}: ActionContext<StateComment, RootState>, payload: any) {
        try {
            const res = await api.addComment(payload.restaurantId, payload.comment);
            let comment = res.comment;
            const username = comment.user_id.email.split("@")[0];
            delete comment.user_id;
            comment.username = username;

            commit('ADD_COMMENT', comment);
            return res;
        } catch (e: any) {
            throw e.data;
        }
    }
}

export default {
    namespaced: true,
    state,
    getters,
    mutations,
    actions
}
