import { CategoryDish, Dish, StateCategoryDish } from "@/@types";
import { ActionContext, ActionTree, GetterTree, MutationTree } from "vuex";
import { RootState } from "@/store";
import api from "@/services/api/ApiService";

const state: StateCategoryDish = {
    categoryDish: {
        id: '',
        category: {
            id: '',
            name: '',
            created_at: '',
            updated_at: ''
        },
        dishes: [],
        created_at: '',
        updated_at: ''
    },
    categoriesDishes: []
}

const getters: GetterTree<StateCategoryDish, RootState> = {
    categoryDish: state => state.categoryDish,
    dishes: state => state.categoryDish.dishes,
    categoriesDishes: state => state.categoriesDishes
}

const mutations: MutationTree<StateCategoryDish> = {
    SET_CATEGORY_DISHES(state: StateCategoryDish, categoryDish: CategoryDish) {
        state.categoryDish = categoryDish;
    },
    SET_CATEGORIES_DISHES(state: StateCategoryDish, categoriesDishes: CategoryDish[]) {
        state.categoriesDishes = categoriesDishes;
    },
    ADD_CATEGORY_DISHES(state: StateCategoryDish, categoryDishes: Dish[]) {
        state.categoryDish.dishes.push(categoryDishes[0]);
    },
    DELETE_CATEGORY_DISHES(state: StateCategoryDish, deletedDishId: String) {
        // @ts-ignore
        const dishToDelete: Dish = state.categoryDish.dishes.find(dish => dish.id === deletedDishId);
        const dishIndexToDelete = state.categoryDish.dishes.indexOf(dishToDelete);
        state.categoryDish.dishes.splice(dishIndexToDelete, 1);
    }
}

const actions: ActionTree<StateCategoryDish, RootState> = {
    async getCategoryDishes({ commit }: ActionContext<StateCategoryDish, RootState>, payload) {
        const res = await api.getCategoryDishes(payload.restaurantId, payload.categoryId);
        commit('SET_CATEGORY_DISHES', res.category_dishes);
        return res;
    },
    async getAllCategoriesDishes({ commit }: ActionContext<StateCategoryDish, RootState>, restaurantId: string) {
        const res = await api.getAllCategoryDishes(restaurantId);
        commit('SET_CATEGORIES_DISHES', res.categories_dishes);
    },
    async addCategoryDishes({ commit }: ActionContext<StateCategoryDish, RootState>, payload) {
        const res = await api.addCategoryDishes(payload.restaurantId, payload.categoryId, payload.dishesId);
        commit('ADD_CATEGORY_DISHES', res.category_dishes);
        return res;
    },
    async deleteCategoryDishes({ commit }: ActionContext<StateCategoryDish, RootState>, payload) {
        const res = await api.deleteCategoryDishes(payload.restaurantId, payload.categoryId, payload.dishId);
        commit('DELETE_CATEGORY_DISHES', res.deleted_category_id);
        return res;
    }
}

export default {
    namespaced: true,
    state,
    getters,
    mutations,
    actions
}
