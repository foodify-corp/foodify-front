import { StateDish, Dish } from "@/@types";
import { ActionTree, ActionContext, MutationTree, GetterTree } from "vuex";
import { RootState } from "@/store";
import api from '@/services/api/ApiService';

const state: StateDish = {
    dishes: [],
    selectedDish: {
        id: '',
        name: '',
        price: 0,
        is_available: true,
        is_available_time: [],
        created_at: '',
        updated_at: '',
        dishes_categories: [],
        restaurants_id: ''
    }
};

const getters:GetterTree<StateDish, RootState> = {
    dishes: (state: StateDish) => state.dishes,
    lowestDishPrice: (state: StateDish) => {
        return Math.min(...state.dishes.map(dish => dish.price));
    },
    highestDishPrice: (state: StateDish) => {
        return Math.max(...state.dishes.map(dish => dish.price));
    }
}

const mutations:MutationTree<StateDish> = {
    DISHES: (state: StateDish, dishes: Dish[]) => {
        state.dishes = dishes;
    },
    ADD_DISH: (state: StateDish, dish: Dish) => {
        state.dishes.push(dish);
    },
    UPDATE_DISH: (state: StateDish, updatedDish: Dish) => {
        const dishToToUpdate = state.dishes.findIndex(dish => dish.id === updatedDish.id);
        state.dishes.splice(dishToToUpdate, 1, updatedDish);
    },
    DELETE_DISH: (state: StateDish, deletedDishId: string) => {
        const dishToDelete = state.dishes.findIndex(dish => dish.id === deletedDishId);
        state.dishes.splice(dishToDelete, 1);
    }
}

const actions:ActionTree<StateDish, RootState> = {
    async getDishes({commit}: ActionContext<StateDish, RootState>, restaurantId: string) {
        try {
            const res = await api.getDishes(restaurantId);
            commit('DISHES', res.dishes);
        } catch (e: any) {
            throw e.message;
        }
    },
    async addDish({commit}: ActionContext<StateDish, RootState>, payload: any) {
        try {
            const res = await api.addDish(payload.selectedRestaurant.id, payload.dish);
            commit('ADD_DISH', res.dish);
            return res;
        } catch (e: any) {
            throw e.message;
        }
    },
    async updateDish({commit}: ActionContext<StateDish, RootState>, payload: any) {
        try {
            const res = await api.updateDish(payload.selectedRestaurant.id, payload.dish);
            commit('UPDATE_DISH', res.dish);
            return res;
        } catch (e: any) {
            throw e.message;
        }
    },
    async deleteDish({commit}: ActionContext<StateDish, RootState>, payload: any) {
        try {
            const res = await api.deleteDish(payload.restaurantId, payload.dishId);
            commit('DELETE_DISH', res.deleted_dish_id);
            return res;
        } catch (e: any) {
            throw e.message;
        }
    },
}

export default {
  namespaced: true,
  state,
  getters,
  actions,
  mutations,
}
