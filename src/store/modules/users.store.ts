import { StateUser, User, Auth } from "@/@types";
import { ActionTree, ActionContext, MutationTree, GetterTree } from "vuex";
import { RootState } from "@/store";
import api from "@/services/api/ApiService";
import { user } from "@/services/api";
import { userOriginalState } from "@/originalState/user";

const state: StateUser = {
    user: {
        ...userOriginalState
    },
    users: [],
    employee: {
        ...userOriginalState
    },
    employees: [],
    auth: {
        token: '',
        refresh_token: ''
    }
};

const getters: GetterTree<StateUser, RootState> = {
    users: (state: StateUser) => state.users,
    connectedUser: (state: StateUser) => state.user,
    auth: (state: StateUser) => state.auth,
    isConnected: (state: StateUser) => state.user.roles.length > 0,
    owner: (state: StateUser) => state.user.roles.includes("ROLE_OWNER") || state.user.roles.includes("ROLE_ADMIN"),
    isUser: (state: StateUser) => state.user.roles.length === 1 && state.user.roles.includes("ROLE_USER"),
    employees: (state: StateUser) => state.employees,
    waiter: (state: StateUser) => state.user.roles.includes("ROLE_WAITER"),
}

const mutations: MutationTree<StateUser> = {
    LOGIN: (state: StateUser, auth: Auth) => {
        state.auth = auth;
    },
    SIGNUP: (state: StateUser, auth: Auth) => {
        // state.auth = auth;
    },
    LOGOUT: (state: StateUser) => {
        state.auth = {
            token: '',
            refresh_token: ''
        };
        state.user = {
            ...userOriginalState
        }
    },
    SET_USER_DATA: (state: StateUser, user: User) => {
        state.user = user;
    },
    SET_USERS_DATA: (state: StateUser, users: User[]) => {
        state.users = users;
    },
    SET_EMPLOYEES: (state: StateUser, employees: User[]) => {
        state.employees = employees;
    },
    SET_EMPLOYEE: (state: StateUser, employee: User) => {
        state.employees.push(employee);
        state.employee = employee;
    },
    UPDATE_EMPLOYEE: (state: StateUser, employee: User) => {
        const employeeIndexToReplace = state.employees.findIndex(user => user.id === employee.id);
        state.employees.splice(employeeIndexToReplace, 1, employee);
    },
    DELETE_EMPLOYEE: (state: StateUser, employeeId: string) => {
        const employeeIndexToDelete = state.employees.findIndex(user => user.id === employeeId);
        state.employees.splice(employeeIndexToDelete, 1);
    }
}

const actions: ActionTree<StateUser, RootState> = {
    async login({ commit, dispatch }: ActionContext<StateUser, RootState>, payload) {
        try {
            const auth = await api.login(payload);
            commit('LOGIN', auth);
        } catch (e: any) {
            throw e.data;
        }
    },
    async signup({ commit, dispatch }: ActionContext<StateUser, RootState>, payload) {
        try {
            const auth = await api.signup(payload);
            commit('SIGNUP', auth);
        } catch (e: any) {
            throw e.data;
        }
    },
    async logout({ commit }: ActionContext<StateUser, RootState>) {
        try {
            // const auth = await api.logout();
            user.value = {};
            commit('LOGOUT');
        } catch (e: any) {
            throw e.data;
        }
    },
    async refreshToken({ commit }: ActionContext<StateUser, RootState>, refreshToken: string) {
        try {
            const auth = await api.refreshToken({ refresh_token: refreshToken });
            commit('LOGIN', auth);
        } catch (e: any) {
            throw e.data;
        }
    },
    async resetPassword({ commit }: ActionContext<StateUser, RootState>) {
        try {
            const res = await api.resetPassword();
            // commit('SET_USER_DATA', res.user)
        } catch (e: any) {
            throw e.data;
        }
    },
    async userInformation({ commit }: ActionContext<StateUser, RootState>) {
        try {
            const res = await api.getUserInformation();
            commit('SET_USER_DATA', res.user)
        } catch (e: any) {
            throw e.data;
        }
    },
    async updateUser({ commit }: ActionContext<StateUser, RootState>, user: User) {
        try {
            const res = await api.updateUser(user);
            if (res !== null) {
                commit('SET_USER_DATA', res.user);
                return res;
            }
        } catch (e: any) {
            throw e.data;
        }
    },
    async createEmployee({ commit }: ActionContext<StateUser, RootState>, payload: { restaurantId: string, employee: any}) {
        console.log('ok')
        try {
            const res = await api.createEmployee(payload.restaurantId, payload.employee);
            if (res !== null) {
                commit('SET_EMPLOYEE', res.user);
                return res;
            }
        } catch (e: any) {
            throw e.data;
        }
    },
    async updateEmployee({ commit }: ActionContext<StateUser, RootState>, payload: { restaurantId: string, employee: any}) {
        try {
            const res = await api.updateEmployee(payload.restaurantId, payload.employee);
            if (res !== null) {
                commit('UPDATE_EMPLOYEE', res.user);
                return res;
            }
        } catch (e: any) {
            throw e.data;
        }
    },
    async getEmployees({ commit }: ActionContext<StateUser, RootState>, restaurantId: string) {
        try {
            const res = await api.getEmployees(restaurantId);
            if (res !== null) {
                commit('SET_EMPLOYEES', res.users);
            }
        } catch (e: any) {
            throw e.data;
        }
    },
    async getEmployee({ commit }: ActionContext<StateUser, RootState>, payload: { restaurantId: string, employeeId: string }) {
        try {
            const res = await api.getEmployees(payload.restaurantId, payload.employeeId);
            if (res !== null) {
                commit('SET_EMPLOYEE', res.user);
            }
        } catch (e: any) {
            throw e.data;
        }
    },
    async deleteEmployee({ commit }: ActionContext<StateUser, RootState>, payload: { restaurantId: string, employeeId: string }) {
        try {
            const res = await api.deleteEmployee(payload.restaurantId, payload.employeeId);
            if (res !== null) {
                commit('DELETE_EMPLOYEE', res.deleted_user_id);
                return res;
            }
        } catch (e: any) {
            throw e.data;
        }
    }
}

export default {
    namespaced: true,
    state,
    getters,
    actions,
    mutations,
}
