import { Reservation, StateReservation } from "@/@types";
import { ActionTree, ActionContext, MutationTree, GetterTree } from "vuex";
import { RootState } from "@/store";
import api from "@/services/api/ApiService";

const state: StateReservation = {
    reservations: [],
    selectedReservation: {
        id: '',
        reservation_date: '',
        nb_people: null,
        is_validated: null,
        confirmed_at: '',
        status: '',
        table_id: '',
        name: '',
        created_at: '',
        updated_at: ''
    }
};

const getters: GetterTree<StateReservation, RootState> = {
    reservations: (state: StateReservation) => state.reservations,
    selectedReservation: (state: StateReservation) => state.selectedReservation
}

const mutations: MutationTree<StateReservation> = {
    RESERVATIONS(state, reservations) {
        state.reservations = reservations;
    },
    UPDATE_RESERVATION(state, updatedReservation: Reservation) {
        const reservationToUpdate = state.reservations.findIndex(reservation => reservation.id === updatedReservation.id);
        state.reservations.splice(reservationToUpdate,1, updatedReservation);
    },
    SET_CURRENT_RESERVATION(state, reservation) {
        state.selectedReservation = reservation
    }
}

const actions: ActionTree<StateReservation, RootState> = {
    async getReservations({ commit }: ActionContext<StateReservation, RootState>, restaurantId: string) {
        if (!restaurantId) {
            throw { message: 'Please select a restaurant first' }
        }
        // eslint-disable-next-line no-useless-catch
        try {
            const res = await api.getReservations(restaurantId);
            commit('RESERVATIONS', res.reservations);
        } catch (e) {
            throw e;
        }
    },
    async addReservation({ commit }: ActionContext<StateReservation, RootState>, payload) {
        // eslint-disable-next-line no-useless-catch
        try {
            const res = await api.addReservation(payload.restaurantId, payload.reservation);
            commit('RESERVATIONS', res.reservations);
            return res;
        } catch (e) {
            throw e;
        }
    },
    async updateReservation({ commit }: ActionContext<StateReservation, RootState>, payload) {
        if (!payload.restaurantId) {
            throw { message: 'Please select a restaurant first' }
        }
        // eslint-disable-next-line no-useless-catch
        try {
            const res = await api.updateReservation(payload.restaurantId, payload.updatedReservation);
            commit('UPDATE_RESERVATION', res.reservation);
            return res;
        } catch (e) {
            throw e;
        }
    },
    setCurrentReservation({ commit }: ActionContext<StateReservation, RootState>, reservation: Reservation) {
        commit('SET_CURRENT_RESERVATION', reservation)
    }
}

export default {
    namespaced: true,
    state,
    getters,
    actions,
    mutations,
}
