import { Restaurant, StateRestaurant } from "@/@types";
import { ActionTree, ActionContext, MutationTree, GetterTree } from "vuex";
import { RootState } from "@/store";
import api from "@/services/api/ApiService";
import { restaurantOriginalState } from "@/originalState";

const state: StateRestaurant = {
    selectedRestaurant: {
        ...restaurantOriginalState
    },
    ownerRestaurants: [],
    restaurants: [],
    restaurant: {
        ...restaurantOriginalState
    }
};

const getters: GetterTree<StateRestaurant, RootState> = {
    selectedRestaurant: (state: StateRestaurant) => state.selectedRestaurant,
    ownerRestaurants: (state: StateRestaurant) => state.ownerRestaurants,
    restaurants: (state: StateRestaurant) => state.restaurants,
    restaurant: (state: StateRestaurant) => state.restaurant,
    filteredRestaurants: (state: StateRestaurant) => (name: string, country: string) => {
        let filteredRestaurants = state.restaurants.filter((restaurant: Restaurant) => {
            return restaurant.name.toLowerCase().indexOf(name) > -1;
        });
        filteredRestaurants = filteredRestaurants.filter((restaurant: Restaurant) => {
            return restaurant.country.toLowerCase().indexOf(country) > -1 ||
                restaurant.city.toLowerCase().indexOf(country) > -1 ||
                restaurant.kitchen_style.toLowerCase().indexOf(country) > -1
        })
        return filteredRestaurants;
    }
}

const mutations: MutationTree<StateRestaurant> = {
    ADD_RESTAURANT(state, restaurant) {
        state.ownerRestaurants.push(restaurant);
    },
    OWNER_RESTAURANTS(state, restaurants) {
        state.ownerRestaurants = restaurants;
    },
    RESTAURANTS(state, restaurants) {
        state.restaurants = restaurants;
    },
    RESTAURANT(state, restaurant) {
        state.restaurant = restaurant;
    },
    SET_CURRENT_RESTAURANT(state, restaurant) {
        state.selectedRestaurant = restaurant;
    },
    UPDATE_RESTAURANT(state, updatedRestaurant) {
        const restaurantToUpdateIndex = state.ownerRestaurants.findIndex(restaurant => restaurant.id === updatedRestaurant.id);
        state.ownerRestaurants.splice(restaurantToUpdateIndex, 1, updatedRestaurant);
    },
    DELETE_RESTAURANT(state, deletedRestaurantId) {
        const restaurantToRemove = state.ownerRestaurants.findIndex(restaurant => restaurant.id === deletedRestaurantId);
        state.ownerRestaurants.splice(restaurantToRemove, 1);
        state.selectedRestaurant = {
            ...restaurantOriginalState
        }
    }
}

const actions: ActionTree<StateRestaurant, RootState> = {
    async addRestaurant({ commit }: ActionContext<StateRestaurant, RootState>, restaurant: Restaurant) {
        // eslint-disable-next-line no-useless-catch
        try {
            const res = await api.addRestaurant(restaurant);
            commit('ADD_RESTAURANT', res.restaurant);
            return res;
        } catch (e) {
            throw e;
        }
    },
    async uploadFile({ commit }: ActionContext<StateRestaurant, RootState>, payload: any) {
        // eslint-disable-next-line no-useless-catch
        try {
            const res = await api.uploadFile(payload.restaurantId, payload.file);
            commit('UPDATE_RESTAURANT', res.restaurant);
            return res;
        } catch (e) {
            throw e;
        }
    },
    async getRestaurants({ commit }: ActionContext<StateRestaurant, RootState>) {
        // eslint-disable-next-line no-useless-catch
        try {
            const res = await api.getRestaurants();
            commit('RESTAURANTS', res.restaurants);
        } catch (e) {
            throw e;
        }
    },
    async getRestaurant({ commit }: ActionContext<StateRestaurant, RootState>, restaurantId: string) {
        // eslint-disable-next-line no-useless-catch
        try {
            const res = await api.getRestaurant(restaurantId);
            commit('RESTAURANT', res.restaurant);
        } catch (e) {
            throw e;
        }
    },
    async getOwnerRestaurants({ commit }: ActionContext<StateRestaurant, RootState>) {
        // eslint-disable-next-line no-useless-catch
        try {
            const res = await api.getOwnerRestaurants();
            commit('OWNER_RESTAURANTS', res.restaurants);
        } catch (e) {
            throw e;
        }
    },
    setCurrentRestaurant({ commit }: ActionContext<StateRestaurant, RootState>, restaurant: Restaurant) {
        commit('SET_CURRENT_RESTAURANT', restaurant);
    },
    async updateRestaurant({ commit }: ActionContext<StateRestaurant, RootState>, restaurant: Restaurant) {
        // eslint-disable-next-line no-useless-catch
        try {
            const res = await api.updateRestaurant(restaurant);
            commit('UPDATE_RESTAURANT', res.restaurant);
            return res;
        } catch (e) {
            throw e;
        }
    },
    async deleteRestaurant({ commit }: ActionContext<StateRestaurant, RootState>, restaurant: Restaurant) {
        // eslint-disable-next-line no-useless-catch
        try {
            const res = await api.deleteRestaurant(restaurant);
            commit('DELETE_RESTAURANT', res);
            return res;
        } catch (e) {
            throw e;
        }
    }
}

export default {
    namespaced: true,
    state,
    getters,
    actions,
    mutations,
}
