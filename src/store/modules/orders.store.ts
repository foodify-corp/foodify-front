import { StateOrder, Order } from "@/@types";
import { GetterTree } from "vuex";
import { RootState } from "@/store";

const state: StateOrder = {
    orderedDishes: [],
    currentOrder: {
        id: '',
        created_at: "",
        updated_at: "",
        order_type: 'string',
        order_dishes: [],
    },
};

const getters:GetterTree<StateOrder, RootState> = {
    orderedDishes: (state: StateOrder) => state.orderedDishes,
    currentOrder: (state: StateOrder) => state.currentOrder,
}

export default {
    namespaced: true,
    state,
    getters,
  }
