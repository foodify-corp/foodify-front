import { RestaurantTimes, StateRestaurantTimes } from "@/@types";
import { ActionTree, ActionContext, MutationTree, GetterTree } from "vuex";
import { RootState } from "@/store";
import api from "@/services/api/ApiService";
import Restaurants from "@/views/Restaurants.vue";
import { restaurantTimesOriginalState } from "@/originalState";

const state: StateRestaurantTimes = {
    restaurantTimes: {
        id: '',
        mondayOpenAt: null,
        mondayCloseAt: null,
        tuesdayOpenAt: null,
        tuesdayCloseAt: null,
        wednesdayOpenAt: null,
        wednesdayCloseAt: null,
        thursdayOpenAt: null,
        thursdayCloseAt: null,
        fridayOpenAt: null,
        fridayCloseAt: null,
        saturdayOpenAt: null,
        saturdayCloseAt: null,
        sundayOpenAt: null,
        sundayCloseAt: null
    }
};

const getters: GetterTree<StateRestaurantTimes, RootState> = {
    restaurantTimes: state => state.restaurantTimes
}

const mutations: MutationTree<StateRestaurantTimes> = {
    SET_RESTAURANT_TIMES(state: StateRestaurantTimes, restaurantTimes: RestaurantTimes) {
        if (restaurantTimes) {
            state.restaurantTimes = restaurantTimes;
        } else {
            state.restaurantTimes = restaurantTimesOriginalState;
        }
    },
    RESET_RESTAURANT_TIMES(state: StateRestaurantTimes) {
        state.restaurantTimes = restaurantTimesOriginalState;
    }
}

const actions: ActionTree<StateRestaurantTimes, RootState> = {
    async getRestaurantTimes({ commit }: ActionContext<StateRestaurantTimes, RootState>, restaurantId: string) {
        // eslint-disable-next-line no-useless-catch
        try {
            const res = await api.getRestaurantTimes(restaurantId);
            commit('SET_RESTAURANT_TIMES', res.restaurant_times);
        } catch (e) {
            throw e;
        }
    },
    async addRestaurantTimes({ commit }: ActionContext<StateRestaurantTimes, RootState>, payload: { restaurantId: string, restaurantTimes: Partial<RestaurantTimes> }) {
        // eslint-disable-next-line no-useless-catch
        try {
            const res = await api.addRestaurantTimes(payload.restaurantId, payload.restaurantTimes);
            commit('SET_RESTAURANT_TIMES', res.restaurant_times);
        } catch (e) {
            throw e;
        }
    },
    async updateRestaurantTimes({ commit }: ActionContext<StateRestaurantTimes, RootState>, payload: { restaurantId: string, restaurantTimes: Partial<RestaurantTimes> }) {
        // eslint-disable-next-line no-useless-catch
        try {
            const res = await api.updateRestaurantTimes(payload.restaurantId, payload.restaurantTimes);
            commit('SET_RESTAURANT_TIMES', res.restaurant_times);
        } catch (e) {
            throw e;
        }
    },
    resetRestaurantTimes({ commit }: ActionContext<StateRestaurantTimes, RootState>) {
        commit('RESET_RESTAURANT_TIMES');
    }
}

export default {
    namespaced: true,
    state,
    getters,
    actions,
    mutations
}
