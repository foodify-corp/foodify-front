import { StateTable, Table } from "@/@types";
import { ActionContext, ActionTree, GetterTree, MutationTree } from "vuex";
import { RootState } from "@/store";
import api from "@/services/api/ApiService";

const state: StateTable = {
    tables: [],
    selectedTable: {
        id: '',
        table_numero: 0,
        nb_place: 0,
        available: false,
        reservations: [],
        lunch_service: '',
        evening_service: '',
        created_at: '',
        updated_at: ''
    }
}

const getters: GetterTree<StateTable, RootState> = {
    tables: (state: StateTable) => state.tables,
    selectedTable: (state: StateTable) => state.selectedTable
}

const mutations: MutationTree<StateTable> = {
    SET_CURRENT_TABLE(state, table) {
        state.selectedTable = table
    },
    TABLES(state, tables: Table[]) {
        state.tables = tables;
    },
    ADD_TABLE(state, table: Table[]) {
        // TODO Revoir la réponse côté back de l'ajout d'une table
        table.map(item => state.tables.push(item));
    },
    UPDATE_TABLE(state, updatedTable: Table) {
        const tableToUpdateIndex = state.tables.findIndex(table=> table.id === updatedTable.id);
        state.tables.splice(tableToUpdateIndex, 1, updatedTable);
    },
    DELETE_TABLE(state, tableId: string) {
        const tableToDeleteIndex = state.tables.findIndex(table=> table.id === tableId);
        state.tables.splice(tableToDeleteIndex, 1);
    }
}

const actions: ActionTree<StateTable, RootState> = {
    async setCurrentTable({commit}: ActionContext<StateTable, RootState>, table: Table) {
        commit('SET_CURRENT_TABLE', table)
    },
    async tables({commit}: ActionContext<StateTable, RootState>, restaurantId: string) {
        // eslint-disable-next-line no-useless-catch
        try {
            const res = await api.tables(restaurantId);
            commit('TABLES', res.tables);
        } catch (e) {
            throw e;
        }
    },
    async addTable({commit}: ActionContext<StateTable, RootState>, payload: any) {
        // eslint-disable-next-line no-useless-catch
        try {
            const res = await api.addTable(payload.restaurantId, payload.table);
            commit('ADD_TABLE', res.tables);
            return res;
        } catch (e) {
            throw e;
        }
    },
    async updateTable({commit}: ActionContext<StateTable, RootState>, payload: any) {
        // eslint-disable-next-line no-useless-catch
        try {
            const res = await api.updateTable(payload.restaurantId, payload.table);
            commit('UPDATE_TABLE', res.table);
            return res;
        } catch (e) {
            throw e;
        }
    },
    async deleteTable({commit}: ActionContext<StateTable, RootState>, payload: any) {
        // eslint-disable-next-line no-useless-catch
        try {
            const res = await api.deleteTable(payload.restaurantId, payload.tableId);
            commit('DELETE_TABLE', res.deleted_table_id);
            return res;
        } catch (e) {
            throw e;
        }
    }
}

export default {
    namespaced: true,
    state,
    getters,
    mutations,
    actions
}
