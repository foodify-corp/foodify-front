// eslint-disable-next-line no-undef
describe('Dish CRUD', function() {

    it('Create dish', async function(browser) {

        // 1. Variables
        const dish = browser.page.ownerDish();
        const price = Math.random() * 100000;

        await dish.navigate()
            .login(browser.globals.ownerCredential.email, browser.globals.ownerCredential.password);

        dish.waitForElementVisible('@ownerRestaurants')
            .selectRestaurant();

        // const result = browser.findElements('css selector', '.table-row-item', function(res) {
        //     console.log(res.value.length)
        // });
        // console.log(result)

        dish.createDish(price);

        // Assertions
        dish.assert.textContains('table', price.toString())

        browser.end();
    });

    it('Update dish', function(browser) {
        const dish = browser.page.ownerDish();
        const price = Math.random() * 100000;

        dish.navigate()
            .login(browser.globals.ownerCredential.email, browser.globals.ownerCredential.password);

        dish.waitForElementVisible('@ownerRestaurants')
            .selectRestaurant();

        dish.updateDish(price);

        // Assertions
        dish.assert.textContains('table > tbody > tr:first-child', price.toString());

        browser.end();
    });

    it('Delete dish', async function(browser) {
        const dish = browser.page.ownerDish();

        await dish.navigate()
            .login(browser.globals.ownerCredential.email, browser.globals.ownerCredential.password);

        dish.waitForElementVisible('@ownerRestaurants')
            .selectRestaurant();

        const result = await dish.findElements('tr');

        await dish.deleteDish();
        // Assertions
        await dish.expect.elements('@tableRowItem').count.to.equal(result.length -1);

        browser.end();
    });
})