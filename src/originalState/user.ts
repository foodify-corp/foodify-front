export const userOriginalState = {
    id: '',
    email: '',
    roles: [],
    firstname: '',
    lastname: '',
    birthday: '',
    phone: '',
    picture: '',
    is_shown: null,
    facebook_url: '',
    instagram_url: '',
    twitter_url: '',
    created_at: '',
    updated_at: ''
}
