import { restaurantOriginalState } from "@/originalState/restaurant";
import { tableOriginalState } from "@/originalState/table";
import { dishOriginalState } from "@/originalState/dish";
import { reservationOriginalState } from "@/originalState/reservation";
import { restaurantTimesOriginalState } from "@/originalState/restaurantTimes";

export {
    restaurantOriginalState,
    tableOriginalState,
    dishOriginalState,
    reservationOriginalState,
    restaurantTimesOriginalState
}
