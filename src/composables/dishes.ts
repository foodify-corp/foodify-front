import { Dish, CategoryDish } from '@/@types';
import { useStore } from "vuex";
import { computed, onMounted } from "vue";


export const useDishes = () => {
    const store = useStore();
    const dishes = computed<Dish[]>(() => store.getters['users/allDishes'])

    const getDishesByCategory = (dishe_type_name: string): Dish[] => {

        const filteredDishesByCategory: Dish[] = [];
        dishes.value.forEach((element: Dish) => {
            if (element.dishes_categories.includes(dishe_type_name)) {
                filteredDishesByCategory.push(element);
            }
        })
        return filteredDishesByCategory;
    }
    const getAllDishesCategories = (): Record<string, string>[] => {

        const dishesCategories = [
            {
                id: 'xxx-111',
                name: 'Plats chauds'
            },
            {
                id: 'xxx-222',
                name: 'Plats froids'
            },
            {
                id: 'xxx-333',
                name: 'Desserts'
            },
            {
                id: 'xxx-444',
                name: 'Boissons'
            },
        ]
        return dishesCategories;
    }

    onMounted(() => {
        // Call API here
    })

    return { dishes, getDishesByCategory, getAllDishesCategories }
}
