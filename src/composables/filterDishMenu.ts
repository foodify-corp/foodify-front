import { computed } from "vue";
import { useStore } from "vuex";
import { Dish } from "@/@types";
import menu from "@/components/Admin/Menu.vue";

export const filterMenuDishes = () => {
    const store = useStore();

    const menuDish = computed(() => store.getters['menuDish/menuDish']);
    const dishes = computed(() => store.getters['dishes/dishes']);

    const availableDishes = computed(() => {
        return dishes.value.filter((dish: Dish) => {
            if (menuDish.value.dishes.length === 0) return dishes;
            return menuDish.value.dishes.every((menuDish: Dish) => {
                return menuDish.id !== dish.id;
            })
        });
    });

    return { availableDishes }
}
