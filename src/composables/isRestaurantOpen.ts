import { useStore } from "vuex";
import { computed, ref } from "vue";
import { RestaurantTimes } from "@/@types";

export const isRestaurantOpen = (restaurantTimes: RestaurantTimes, reservationDay: string): boolean => {
    const date = computed(() => reservationDay ? new Date(reservationDay) : new Date());
    const day = date.value.getDay();
    const hours = date.value.getHours();
    if (restaurantTimes) {
        switch (day) {
            case 1:
                if (restaurantTimes['mondayOpenAt'] && restaurantTimes['mondayCloseAt']) {
                    return hours >= restaurantTimes['mondayOpenAt'] && hours <= restaurantTimes['mondayCloseAt']
                }
                break;
            case 2:
                if (restaurantTimes['tuesdayOpenAt'] && restaurantTimes['tuesdayCloseAt']) {
                    return hours >= restaurantTimes['tuesdayOpenAt'] && hours <= restaurantTimes['tuesdayCloseAt']
                }
                break;
            case 3:
                if (restaurantTimes['wednesdayOpenAt'] && restaurantTimes['wednesdayCloseAt']) {
                    return hours >= restaurantTimes['wednesdayOpenAt'] && hours <= restaurantTimes['wednesdayCloseAt']
                }
                break;
            case 4:
                if (restaurantTimes['thursdayOpenAt'] && restaurantTimes['thursdayCloseAt']) {
                    return hours >= restaurantTimes['thursdayOpenAt'] && hours <= restaurantTimes['thursdayCloseAt']
                }
                break;
            case 5:
                if (restaurantTimes['fridayOpenAt'] && restaurantTimes['fridayCloseAt']) {
                    return hours >= restaurantTimes['fridayOpenAt'] && hours <= restaurantTimes['fridayCloseAt']
                }
                break;
            case 6:
                if (restaurantTimes['saturdayOpenAt'] && restaurantTimes['saturdayCloseAt']) {
                    return hours >= restaurantTimes['saturdayOpenAt'] && hours <= restaurantTimes['saturdayCloseAt']
                }
                break;
            case 7:
                if (restaurantTimes['sundayOpenAt'] && restaurantTimes['sundayCloseAt']) {
                    return hours >= restaurantTimes['sundayOpenAt'] && hours <= restaurantTimes['sundayCloseAt']
                }
                break;
            default:
                return false;
        }
    }
    return false;
}
