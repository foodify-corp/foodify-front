import { computed } from "vue";
import { useStore } from "vuex";
import { Dish } from "@/@types";

export const filterCategoryDishes = () => {
    const store = useStore();

    const categoryDish = computed(() => store.getters['categoryDish/categoryDish']);
    const dishes = computed(() => store.getters['dishes/dishes']);

    const availableDishes = computed(() => {
        return dishes.value.filter((dish: Dish) => {
            if (categoryDish.value.dishes.length === 0) return dishes;
            return categoryDish.value.dishes.every((categoryDish: Dish) => {
                return categoryDish.id !== dish.id;
            })
        });
    });

    return { availableDishes }
}
