export default (width: number = 200, length: number = 200) => {
    const random = Math.floor(Math.random() * 200);
    return `https://picsum.photos/id/${random}/${width}/${length}`;
}
