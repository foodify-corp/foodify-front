import store from '@/store';
import { Dish } from '@/@types';
import { OrderDish } from '@/@types/OrderDish';
import { Order } from '@/@types/Order';

export const getCurrentOrder = (): Order => {
    const currentOrder = store.getters['orders/currentOrder'];
    return currentOrder;
}

export const addToCurrentOrder = (dish: Dish): Order => {
    const currentOrder: Order = getCurrentOrder();
    const orderDish: OrderDish[] = currentOrder.order_dishes;
    const orderDishToAdd: OrderDish = {
        quantity: 1,
        dish: dish,
        created_at: '',
        updated_at: '',
    }
    const idDishesAlreadyOrdered = orderDish.map(x => x.dish.id);

    if (!idDishesAlreadyOrdered.includes(dish.id)) {
        orderDish.push(orderDishToAdd);
    } else {
        orderDish.forEach((element: OrderDish) => {
            if (element.dish.id === dish.id) {
                element.quantity = element.quantity + 1;
            }
        } )
    }

    return currentOrder;
}

export const substituteToCurrentOrder = (dish: Dish, index: number): Order => {
    const currentOrder = getCurrentOrder();
    const orderDish = currentOrder.order_dishes;
    orderDish.forEach((element: OrderDish) => {
        if (element.dish.id === dish.id) {
            if (element.quantity < 2) {
                deleteFromCurrentOrder(index)
            } else {
                element.quantity = element.quantity - 1;
            }
        }
    } )
    return currentOrder;
}

export const deleteFromCurrentOrder = (index: number): OrderDish[] => {
    const orderDish = getCurrentOrder().order_dishes;
    orderDish.splice(index, 1);
    return orderDish;
}

export const resetCurrentOrder = () => {
    const currentOrder: Order = getCurrentOrder();
    currentOrder.order_dishes = [];
    currentOrder.order_type = '';
}

export const sortedDishesAccordingToAvailability = (dishes: Dish[]) => {
    return dishes.sort((a, b) => Number(b.is_available) - Number(a.is_available));
}

export const getTotalPriceOfCurrentOrder = () => {
    const currentOrder = store.getters['orders/currentOrder'];
    let totalPrice = 0;
    currentOrder.order_dishes.forEach((element: OrderDish) => {
        totalPrice += element.quantity * element.dish.price;
    })
    return totalPrice;
}

export const setOrderType = (orderType: string) => {
    const currentOrder: Order = getCurrentOrder();
    currentOrder.order_type = orderType;
}
