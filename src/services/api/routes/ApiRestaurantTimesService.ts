import { ApiBaseService } from "@/services/api/ApiService";
import { RestaurantTimes } from "@/@types";

export const ApiRestaurantTimesService = (superclass: typeof ApiBaseService) => class extends superclass {

    async getRestaurantTimes(restaurantId: string) {
        const uri = `/restaurant/${restaurantId}/restaurant-times`;
        try {
            const res = await this.client().get(uri);
            return res.data;
        } catch (e: any) {
            throw e.response;
        }
    }

    async addRestaurantTimes(restaurantId: string, restaurantTimes: RestaurantTimes) {
        const uri = `/restaurant/${restaurantId}/restaurant-times`;
        try {
            const res = await this.client().post(uri, restaurantTimes);
            return res.data;
        } catch (e: any) {
            throw e.response;
        }
    }

    async updateRestaurantTimes(restaurantId: string, restaurantTimes: RestaurantTimes) {
        const uri = `/restaurant/${restaurantId}/restaurant-times`;
        try {
            const res = await this.client().put(uri, restaurantTimes);
            return res.data;
        } catch (e: any) {
            throw e.response;
        }
    }
}
