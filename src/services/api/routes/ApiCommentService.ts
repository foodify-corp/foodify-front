import { ApiBaseService } from "@/services/api/ApiService";
import { Comment } from "@/@types";

export const ApiCommentService = (superclass: typeof ApiBaseService) => class extends superclass {

    async getComments(restaurantId: string) {
        const uri = `/restaurant/${restaurantId}/comment`;
        try {
            const res = await this.client().get(uri);
            return res.data;
        } catch (e: any) {
            throw e.response;
        }
    }

    async addComment(restaurantId: string, comment: Comment) {
        const uri = `/restaurant/${restaurantId}/comment`;
        try {
            const res = await this.client().post(uri, comment);
            return res.data;
        } catch (e: any) {
            throw e.response;
        }
    }
}
