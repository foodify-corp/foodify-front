import { ApiBaseService } from "@/services/api/ApiService";

export const ApiCategoryDishService = (superclass: typeof ApiBaseService) => class extends superclass {

    async getCategoryDishes(restaurantId: string, categoryId: string) {
        const uri = `/restaurant/${restaurantId}/category/${categoryId}/category-dish`;
        try {
            const res = await this.client().get(uri);
            return res.data;
        } catch (e: any) {
            throw e.response;
        }
    }

    async getAllCategoryDishes(restaurantId: string) {
        const uri = `/restaurant/${restaurantId}/category-dish`;
        try {
            const res = await this.client().get(uri);
            return res.data;
        } catch (e: any) {
            throw e.response;
        }
    }

    async addCategoryDishes(restaurantId: string, categoryId: string, dishesId: String[]) {
        const uri = `/restaurant/${restaurantId}/category/${categoryId}/category-dish`;
        try {
            const res = await this.client().post(uri, dishesId);
            return res.data;
        } catch (e: any) {
            throw e.response;
        }
    }

    async deleteCategoryDishes(restaurantId: string, categoryId: string, categoryDishesId: string) {
        const uri = `/restaurant/${restaurantId}/category/${categoryId}/category-dish/${categoryDishesId}`;
        try {
            const res = await this.client().delete(uri);
            return res.data;
        } catch (e: any) {
            throw e.response;
        }
    }
}
