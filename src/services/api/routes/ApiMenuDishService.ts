import { ApiBaseService } from "@/services/api/ApiService";

export const ApiMenuDishService = (superclass: typeof ApiBaseService) => class extends superclass {

    async getMenuDishes(restaurantId: string, menuId: string) {
        const uri = `/restaurant/${restaurantId}/menu/${menuId}/menu-dish`;
        try {
            const res = await this.client().get(uri);
            return res.data;
        } catch (e: any) {
            throw e.response;
        }
    }

    async getAllMenuDishes(restaurantId: string) {
        const uri = `/restaurant/${restaurantId}/menu-dishes`;
        try {
            const res = await this.client().get(uri);
            return res.data;
        } catch (e: any) {
            throw e.response;
        }
    }

    async addMenuDishes(restaurantId: string, menuId: string, dishesId: String[]) {
        const uri = `/restaurant/${restaurantId}/menu/${menuId}/menu-dish`;
        try {
            const res = await this.client().post(uri, dishesId);
            return res.data;
        } catch (e: any) {
            throw e.response;
        }
    }

    async deleteMenuDishes(restaurantId: string, menuId: string, menuDishesId: string) {
        const uri = `/restaurant/${restaurantId}/menu/${menuId}/menu-dish/${menuDishesId}`;
        try {
            const res = await this.client().delete(uri);
            return res.data;
        } catch (e: any) {
            throw e.response;
        }
    }
}
