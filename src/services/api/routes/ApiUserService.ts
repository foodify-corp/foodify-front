import { ApiBaseService } from "@/services/api/ApiService";
import { User } from "@/@types";

export const ApiUserService = (superclass: typeof ApiBaseService) => class extends superclass {

    getAllUsers = async (): Promise<User[]> => {
        const uri = '/users';
        try {
            const response = await this.client().get(uri);
            return response.data.users;
        } catch (e: any) {
            throw e.response;
        }
    }

    getUserInformation = async (): Promise<User> => {
        const uri = '/users/me';
        try {
            const response = await this.client().get(uri);
            return response.data;
        } catch (e: any) {
            throw e.response;
        }
    }

    updateUser = async (user: User): Promise<User> => {
        const uri = '/users/update-user';
        try {
            const response = await this.client().put(uri, user);
            return response.data;
        } catch (e: any) {
            throw e.response;
        }
    }

    resetPassword = async (): Promise<any> => {
        const uri = '/reset-password';
        try {
            const response = await this.client().post(uri);
            return response.data;
        } catch (e: any) {
            throw e.response;
        }
    }

    createEmployee = async (restaurantId: string, employee: { email: string, roles: string[] }) => {
        console.log(restaurantId, employee)
        const uri = `/restaurant/${restaurantId}/generate_employee`;
        try {
            const response = await this.client().post(uri, employee);
            return response.data;
        } catch (e: any) {
            throw e.response;
        }
    }

    getEmployees = async (restaurantId: string): Promise<User[]> => {
        const uri = `/restaurant/${restaurantId}/user-restaurant`;
        try {
            const response = await this.client().get(uri);
            return response.data;
        } catch (e: any) {
            throw e.response;
        }
    }

    getEmployee = async (restaurantId: string, employeeId: string): Promise<User> => {
        const uri = `/restaurant/${restaurantId}/user-restaurant/${employeeId}`;
        try {
            const response = await this.client().get(uri);
            return response.data;
        } catch (e: any) {
            throw e.response;
        }
    }

    deleteEmployee = async (restaurantId: string, employeeId: string): Promise<User> => {
        const uri = `/restaurant/${restaurantId}/user-restaurant/${employeeId}`;
        try {
            const response = await this.client().delete(uri);
            return response.data;
        } catch (e: any) {
            throw e.response;
        }
    }

    updateEmployee = async (restaurantId: string, employee: User): Promise<User> => {
        const uri = `/restaurant/${restaurantId}/user-restaurant/${employee.id}`;
        try {
            const response = await this.client().put(uri, employee);
            return response.data;
        } catch (e: any) {
            throw e.response;
        }
    }
}
