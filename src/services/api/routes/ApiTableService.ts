import { ApiBaseService } from "@/services/api/ApiService";
import { Table } from "@/@types";

export const ApiTableService = (superclass: typeof ApiBaseService) => class extends superclass {

    async tables(restaurantId: string) {
        try {
            const uri = `/restaurant/${restaurantId}/table`;
            const res = await this.client().get(uri);
            return res.data;
        } catch (e: any) {
            throw e.response;
        }
    }

    async addTable(restaurantId: string, table: Table) {
        try {
            const uri = `/restaurant/${restaurantId}/table`;
            const res = await this.client().post(uri, table);
            return res.data;
        } catch (e: any) {
            throw e.response;
        }
    }

    async updateTable(restaurantId: string, table: Table) {
        try {
            const uri = `/restaurant/${restaurantId}/table/${table.id}`;
            const res = await this.client().put(uri, table);
            return res.data;
        } catch (e: any) {
            throw e.response;
        }
    }

    async deleteTable(restaurantId: string, tableId: string) {
        try {
            const uri = `/restaurant/${restaurantId}/table/${tableId}`;
            const res = await this.client().delete(uri);
            return res.data;
        } catch (e: any) {
            throw e.response;
        }
    }
}
