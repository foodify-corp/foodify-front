import { useStorage } from "@vueuse/core";
import { computed, Ref } from "vue";
import axios, { AxiosInstance } from "axios";

export const user = useStorage<any>('user', {}) as Ref<Record<string, string>>;

const config = computed(() => {
    return {
        baseURL: import.meta.env.VITE_APP_API_BASE_URL,
        headers: {
            'Content-Type': 'application/json'
        }
    }
});

const client: AxiosInstance = axios.create(config.value);

client.interceptors.request.use(function (config: any) {
    config.headers.Authorization =  user.value.token ? user.value.token : '';
    return config;
});

export default client
