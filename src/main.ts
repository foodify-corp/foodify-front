import { createApp } from 'vue'
import App from './App.vue'
import router from './router'
import store from './store'
import './registerServiceWorker'
import 'mosha-vue-toastify/dist/style.css'
import './assets/styles/button.css'
import './assets/styles/card.css'
import './assets/styles/input.css'
import './tailwind.css'

createApp(App).use(store).use(router).mount('#app');
