import { useStorage } from "@vueuse/core";
import { Ref } from "vue";

export const cardLayoutConfig = useStorage<any>('cardLayoutConfig', true) as Ref<boolean>;
