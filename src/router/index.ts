import { createRouter, createWebHistory, RouteRecordRaw } from 'vue-router'
import Home from '../views/Home.vue'
import Login from '../views/Login.vue'
import Signup from '../views/Signup.vue'
import Restaurants from '../views/Restaurants.vue'
import RestaurantSingle from '../views/Restaurant.vue'
import DashboardAdmin from '../views/Admin/DashboardAdmin.vue'
import DashboardWaiter from '../views/Waiter/DashboardWaiter.vue'
import Restaurant from '../views/Admin/Restaurant.vue'
import Reservation from '../views/Admin/Reservation.vue'
import DishMenuCategory from '../views/Admin/DishMenuCategory.vue'
import Profile from '../views/Profile.vue'
import Table from '../views/Admin/Table.vue'
import UsersRestaurant from '../views/Admin/UsersRestaurant.vue'
import Order from '../views/Waiter/Order.vue'
import PageNotFound from '../views/NotFound.vue'
import store from '../store/index';
import { computed } from "vue";
import { useStorage } from "@vueuse/core";

const owner = computed(() => store.getters['users/owner']);
const waiter = computed(() => store.getters['users/waiter']);
export const lastVisitedRoute: any = useStorage('lastVisitedRoute', '');
const isAuth = computed(() => store.getters['users/isConnected']);

const routes: Array<RouteRecordRaw> = [
    {
        path: '/',
        name: 'Home',
        component: Home
    },
    {
        path: '/login',
        name: 'Login',
        component: Login
    },
    {
        path: '/signup',
        name: 'Signup',
        component: Signup
    },
    {
        path: '/profile',
        name: 'Profile',
        component: Profile,
        meta: { isAuth: true }
    },
    {
        path: '/restaurants',
        name: 'RestaurantsList',
        component: Restaurants
    },
    {
        path: '/restaurants/:id',
        name: 'Restaurant',
        component: RestaurantSingle
    },
    {
        path: '/waiter',
        name: 'Waiter',
        component: DashboardWaiter,
        // meta: { isWaiter: true },
        children: [
            {
                path: 'order',
                name: 'Order',
                component: Order
            }
        ]
    },
    {
        path: '/owner',
        name: 'Owner',
        component: DashboardAdmin,
        meta: { isOwner: true },
        children: [
            {
                path: 'restaurant',
                name: 'ManageRestaurant',
                component: Restaurant
            },
            {
                path: 'reservation',
                name: 'ManageReservation',
                component: Reservation
            },
            {
                path: 'dishMenuCategory',
                name: 'ManageDishMenuCategory',
                component: DishMenuCategory
            },
            {
                path: 'table',
                name: 'ManageTable',
                component: Table
            },
            {
                path: 'usersRestaurant',
                name: 'ManageUsersRestaurant',
                component: UsersRestaurant
            },
        ]
    },
    {
        path: '/404',
        name: 'PageNotExist',
        component: PageNotFound
    },
    {
        path: "/:catchAll(.*)",
        redirect: '/404'
    },
]

const router = createRouter({
    history: createWebHistory(import.meta.env.BASE_URL),
    routes
})

router.beforeEach((to, from) => {
    // Check if user is connected
    // Route backend bearer token userId
    if (to?.meta?.isAuth && !isAuth.value) {
        return {
            path: '/',
            // save the location we were at to come back later
            query: { redirect: to.fullPath },
        }
    }
    if (to?.meta?.isOwner && !owner.value) {
        return {
            path: '/',
            // save the location we were at to come back later
            query: { redirect: to.fullPath },

        }
    }

    // if(to?.meta?.isWaiter && !waiter.value) {
    //     return {
    //         path: '/',
    //         // save the location we were at to come back later
    //         query: { redirect: to.fullPath },

    //     }
    // }
});

router.afterEach(to => {
    lastVisitedRoute.value = to.name;
});

export default router
