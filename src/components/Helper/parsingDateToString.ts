export const parsingDateToString = (string: string) => {
    return new Date(string).toLocaleString('fr', { day: 'numeric', month: 'long', year: 'numeric', hour: 'numeric', minute: 'numeric' });
}
